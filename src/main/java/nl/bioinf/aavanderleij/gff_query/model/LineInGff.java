package nl.bioinf.aavanderleij.gff_query.model;

/**
 *
 */

public class LineInGff {
    private String seqid;
    private String source;
    private String type;
    private int start;
    private int end;
    private String score;
    private String strand;
    private String phase;
    private String attributes;
    private String line_in_file;
    private Integer length;

    public  LineInGff(String line_in_file){
        parseLine(line_in_file);






    }

    public void parseLine(String line_in_file){
        String[] colloms_in_line = line_in_file.split("\\t");
        this.seqid = colloms_in_line[0];
        this.source = colloms_in_line[1];
        this.type = colloms_in_line[2];
        this.start = Integer.parseInt(colloms_in_line[3]);
        this.end = Integer.parseInt(colloms_in_line[4]);
        this.score = colloms_in_line[5];
        this.strand = colloms_in_line[6];
        this.phase = colloms_in_line[7];
        this.attributes = colloms_in_line[8];
        this.line_in_file = line_in_file;
        this.length = this.end - this.start;

    }

    public String getType(){
        return this.type;
    }

    public String getPhase(){
        return this.phase;
    }

    public String getSeqid(){
        return this.seqid;
    }

    public String getSource(){
        return this.source;
    }

    public int getStart(){
        return this.start;
    }

    public int getEnd(){
        return this.end;
    }

    public String getScore(){
        return this.score;
    }

    public String getStrand(){
        return this.strand;
    }

    public String getAttributes(){
        return this.attributes;
    }

    public String getLine_in_file() {return this.line_in_file;}

    public Integer getLength(){return this.length;}


}
