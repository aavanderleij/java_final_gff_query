package nl.bioinf.aavanderleij.gff_query.model;

import java.util.ArrayList;

public class GffQueryFilter {

    public void GffQueryFilter(){

    }



    public ArrayList<LineInGff> fetchType(String wanted_type,ArrayList list_with_line_objects){
        // make a list for the objects with the right type
        ArrayList<LineInGff> filterd_list = new ArrayList<>();
        // for loop
        for (int i = 0; i < list_with_line_objects.size(); i++) {
            LineInGff single_line_object = (LineInGff) list_with_line_objects.get(i);
            // if the type of the line object equals the wanted type
            if (single_line_object.getType().equalsIgnoreCase(wanted_type)){

                filterd_list.add(single_line_object);

            }
        }
        System.out.println(filterd_list);
        return filterd_list;
    }

    public ArrayList<LineInGff> fetchRegion(String wanted_region, ArrayList list_with_line_objects){
        // make a list for the objects with the right type
        ArrayList<LineInGff> filterd_list = new ArrayList<>();
        // split the wanted_region coordinates to a start and end
        String[] coordinates = wanted_region.split("\\.\\.");

        int start = Integer.parseInt(coordinates[0]);
        int end = Integer.parseInt(coordinates[1]);

        // for loop
        for (int i = 0; i < list_with_line_objects.size(); i++){
            LineInGff single_line_object = (LineInGff) list_with_line_objects.get(i);
            // check if stat is equal or bigger then object start and if end is equal or smaller then object end
            if (single_line_object.getStart() >= start & single_line_object.getEnd() <= end){
                // add the object to the filtered list
                filterd_list.add(single_line_object);
            }
        }
        return filterd_list;
    }

    public ArrayList<LineInGff> filter (String filter, ArrayList list_with_line_objects){

        // make a list for the objects with the right type
        // split the filter to SOURCE, SCORE, ORIENTATION MAXIMUM AND/OR MINIMUM LENGTH

        String[] filters = filter.split("\\|");
        String source = filters[0];
        String score = filters[1];

        String orientation = filters[2];
        String minimum_length = filters[3];
        String maximum_length = filters[4];
        
        // make list to remove
        ArrayList<LineInGff> no_match = new ArrayList<>();
        


        for (int i = 0; i < list_with_line_objects.size(); i++) {
            LineInGff single_line_object = (LineInGff) list_with_line_objects.get(i);
            if (source.equals("*")) {

                // do nothing
            } else if (! single_line_object.getSource().equals(source)) {
                no_match.add(single_line_object);

            }

            if (score.equals("*")) {
                // do nothing

            } else if (! single_line_object.getScore().equals(score)) {
                no_match.add(single_line_object);

            }

            if (orientation.equals("*")) {
                // do nothing

            } else if (! single_line_object.getStrand().equals(orientation)) {
                no_match.add(single_line_object);

            }


            if (minimum_length.equals("*")) {
                // do nothing
            } else if (single_line_object.getLength() <= Integer.parseInt(minimum_length)) {
                no_match.add(single_line_object);
            }

            if (maximum_length.equals("*")) {
                // do nothing
            } else if (single_line_object.getLength() >= Integer.parseInt(maximum_length)) {
                no_match.add(single_line_object);

            }
        }

        list_with_line_objects.removeAll(no_match);
        return list_with_line_objects;
    }
}
