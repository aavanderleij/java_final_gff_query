package nl.bioinf.aavanderleij.gff_query.IO;



import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

public class FileReader {
    public ArrayList FileReader(Path input_file_path) {
        ArrayList linesFromFile = new ArrayList<String>();
        try (BufferedReader reader = Files.newBufferedReader(input_file_path, Charset.defaultCharset())){
            String lineFromFile;
            while ((lineFromFile = reader.readLine()) != null){
                linesFromFile.add(lineFromFile);
            }

        }
            catch (IOException exception){
                System.out.println("Error");
            }
            // print file contents
            System.out.println(linesFromFile);
            // return lines from file
            return (linesFromFile);
        }




    }



