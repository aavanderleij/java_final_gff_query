package nl.bioinf.aavanderleij.gff_query.IO;


import org.apache.commons.cli.*;


public class CLIParser {
    // define options
    public Options options;
    // define parser
    public CommandLineParser parser;
    public CLIParser() {

        // create options
        this.options = new Options();
        // create parser
        this.parser = new DefaultParser();

        options.addOption("s", "summery", true, "give a summery of the file");
        options.addOption("t", "fetch_type", true,
                "Will fetch a certain type of feature (e.g. gene, CDS)");
        options.addOption("r", "fetch_region", true,
                "Will select all features that are included completely within the given coordinates");
        options.addOption("f", "filter", true,
                "<SOURCE|SCORE|ORIENTATION|MAXIMUM|MINIMUM LENGTH>" +
                        " Will filter on any of these data fields, in any combination.\n To suppress a filter fil in a * sine");
        options.addOption("i", "input_file", true, "input file");

    }


    public Object cliParse(String[] commandLineArgs) {
        Object commandLineInput = new Object();
        try {
            // parse the command line arguments
            commandLineInput = this.parser.parse(this.options, commandLineArgs);

        }
        catch( ParseException exp ) {

            System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
        }
        return commandLineInput;
    }




    }

