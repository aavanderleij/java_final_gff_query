package nl.bioinf.aavanderleij.gff_query;


import nl.bioinf.aavanderleij.gff_query.IO.CLIParser;
import nl.bioinf.aavanderleij.gff_query.IO.FileReader;
import nl.bioinf.aavanderleij.gff_query.model.GffQueryFilter;
import nl.bioinf.aavanderleij.gff_query.model.LineInGff;
import org.apache.commons.cli.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class GffQuery {
    public static void main(String[] commandLineArgs) {
        // test if the main is running (I like hello world, makes me happy)
        System.out.println("Hello world!");
        // call CLIParser to get command line arguments
        CLIParser comandLineparser = new CLIParser();
        // get the command line input from CLI parser
        CommandLine commandLineInput = (CommandLine) comandLineparser.cliParse(commandLineArgs);
        // get the input file from the command line
        String input_file = commandLineInput.getOptionValue("i");
        // see if fetch type is asked

        // make path from string
        Path input_file_path = Paths.get(input_file);

        // make fileReader object
        FileReader fileReader = new FileReader();

        // make filter object
        GffQueryFilter filter = new GffQueryFilter();
        // Read file
        ArrayList<String> input_file_list = fileReader.FileReader(input_file_path);

        // make a list for the LineInGff objects
        ArrayList<LineInGff> list_with_line_objects = new ArrayList<LineInGff>();

        // for every line make a LineInGff object and add to array list
        for (int i = 0; i < input_file_list.size(); i++) {
            if (i == 0){
                String header = input_file_list.get(i);

            }
            else {
            LineInGff line = new LineInGff(input_file_list.get(i));
            list_with_line_objects.add(line);
        }}

        if (commandLineInput.hasOption("t")) {
            String wanted_type = commandLineInput.getOptionValue("t");
            System.out.println("option fetch_type found " + wanted_type);
            System.out.println("list_with_line_objects = " + list_with_line_objects.size());
            list_with_line_objects = filter.fetchType(wanted_type,list_with_line_objects);
            System.out.println("list_with_line_objects = " + list_with_line_objects.size());



        }

        if (commandLineInput.hasOption("r")) {
            String wanted_region = commandLineInput.getOptionValue("r");
            System.out.println("option fetch_region found " + wanted_region);
            list_with_line_objects = filter.fetchType(wanted_region,list_with_line_objects);

        }

        if (commandLineInput.hasOption("f")) {
            String wanted_filter = commandLineInput.getOptionValue("f");
            System.out.println("option filter found " + wanted_filter);
            System.out.println("list_with_line_objects = " + list_with_line_objects.size());
            list_with_line_objects = filter.filter(wanted_filter,list_with_line_objects);

            System.out.println("list_with_line_objects = " + list_with_line_objects.size());




        }
}}
