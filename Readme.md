##readme

Final assignment 2: Developing a GFF file query app
Special challenge of this assignment: applying and combining filters
Assignment details
The GFF3 file format is a data format used to communicate the presence and location of feature on a sequence.
GFF3 stands for Generic Feature Format version three.
The fields are these:
seqname - The name of the sequence. Typically a chromosome or a contig.
source - The program that generated this feature.
feature - The name of this type of feature. The official GFF3 spec states that this should be a term from the SOFA ontology.
start - The starting position of the feature in the sequence. The first base is numbered 1.
end - The ending position of the feature (inclusive).
score - A score between 0 and 1000. If there is no score value, enter ".".
strand - Valid entries include '+', '-', or '.' (for don't know/don't care).
frame - If the feature is a coding exon, frame should be a number between 0-2 that represents the reading frame of the first base. If the feature is not a coding exon, the value should be '.'.
GFF3: grouping attributes Attribute keys and values are separated by '=' signs. Values must be URI encoded.quoted. Attribute pairs are separated by semicolons. Certain, special attributes are used for grouping and identification (See below). This field is the one important difference between GFF flavors.
There are example files in the "data" folder of this project.

At this moment the only tings that are implemented are the CLI parser and the file reader.

To use this program use the following command
java GffQuery.java -i <path to file>


In the future the following methods wil be implemented:

--fetch_type <TYPE> Will fetch a certain type of feature (e.g. gene, CDS)
--fetch_region <COORDINATES> Will select all features that are included completely within the given coordinates
--filter <SOURCE, SCORE, ORIENTATION MAXIMUM AND/OR MINIMUM LENGTH> Will filter on any of these data fields, in any combination:

